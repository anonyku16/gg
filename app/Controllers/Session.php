<?php namespace App\Controllers;

use App\Controllers\Pubcon_user;

class Session extends BaseController
{

    public function apiRequest($url, $post=FALSE, $headers=array()) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        if($post)
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $headers[] = 'Accept: application/json';
        if($this->session('access_token'))
            $headers[] = 'Authorization: Bearer ' . $this->session('access_token');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        return json_decode($response);
    }

    function get($key, $default=NULL) {
        return array_key_exists($key, $_GET) ? $_GET[$key] : $default;
    }

    function session($key, $default=NULL) {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : $default;
    }

	public function register(){
        $pubcon_user = new Pubcon_user();
        $data_user = array();

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes. In case if your CURL is slow and is loading too much (Can be IPv6 problem)
        error_reporting(E_ALL);
        define('OAUTH2_CLIENT_ID', '625386197044297756');
        define('OAUTH2_CLIENT_SECRET', 'dfPZ-KEqQaSGJWyQ0lftefPCKrFZWmig');
        $authorizeURL   = 'https://discordapp.com/api/oauth2/authorize?client_id=625386197044297756&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fregister&response_type=code&scope=identify%20email%20connections%20guilds%20guilds.join%20rpc';
        $tokenURL       = 'https://discordapp.com/api/oauth2/token';
        $apiURLBase     = 'https://discordapp.com/api/users/@me';

        session_start();
        

        // IF GET CODE FROM AUTHENTICATION WITH DISCORD
        //if(ISSET($_GET["code"])){
        if($this->get('code')) {

            $token = $this->apiRequest($tokenURL, array(
                "grant_type" => "authorization_code",
                'client_id' => OAUTH2_CLIENT_ID,
                'client_secret' => OAUTH2_CLIENT_SECRET,
                'redirect_uri' => 'http://localhost:8080/register',
                'code' => $this->get('code')
            ));

            // IF REFRESH TOKEN VANISHED
            if(!ISSET($token->access_token)){
                return redirect("/");
            }

            else{
                $logout_token = $token->access_token;
                $_SESSION['access_token'] = $token->access_token; 
                
                return redirect("register");
            }
            
        }

        // IF NOT GET CODE FROM AUTHENTICATION WITH DISCORD OR REFRESH TOKEN VANISHED
        else{
            if(ISSET($_SESSION['access_token'])){
                $user = $this->apiRequest($apiURLBase);
                $_SESSION['email_oauth2'] = $user->email;
                $_SESSION['id_oauth2'] = $user->id;

                //QRY CHECKING REGISTERED USER
                $data_user = $pubcon_user->getByEmail($user->email);
                if($data_user && !$data_user instanceof Exception){
                    //USER ALREADY REGISTERED
                   echo "<script>
                    alert('Anda Telah Terdaftar. Client Area Akan Segera Hadir !');
                    window.location.href='https://discord.gg/dAnD9T7';
                    </script>";
                }

                else{

                    $dataarray  = array(
                                    "vd_email" => $user->email,
                                );

                    //USER NOT REGISTERED YET
                    return view('v_fe_registerpage',$dataarray);
                }
            }

            else{
                return redirect("/");
            }
        }
       
        
    }

    public function saveRegister(){
        session_start();

        /*
        echo $this->request->getVar('i_user_name')."<br>";
        echo $_SESSION['access_token']."<br>";
        echo $_SESSION['email_oauth2']."<br>";
        */

        $user_accesstoken   = $_SESSION['access_token'];
        $user_name          = $this->request->getVar('i_user_name');
        $user_email         = $_SESSION['email_oauth2'];
        $user_discordid     = $_SESSION['id_oauth2'];
        $user_level         = "0";
        $user_role_id       = 2;

        $user = new Pubcon_user();
        $user->setuserAccessToken($user_accesstoken);
        $user->setuserDiscordId($user_discordid);
        $user->setuserName($user_name);
        $user->setuserEmail($user_email);
        $user->setuserLevel($user_level);
        $user->setuserRole_Id($user_role_id);

        $data = $user->add();

        if(!$data instanceof Exception){
            // IF SUCCESS REGISTER
            if($data['user_id']){
                echo "<script>
                alert('Berhasil Melakukan Registrasi Member');
                window.location.href='https://discord.gg/dAnD9T7';
                </script>";
            }

            else{
                echo "<script>
                alert('ERROR ! Please Contact Administrator');
                window.location.href='/';
                </script>";
            }
        }

        else{
            echo "<script>
            alert('ERROR ! Please Contact Administrator');
            window.location.href='/';
            </script>";
        }
    }
    
    public function logout(){

        session_start();
        
        // remove all session variables
        session_unset();

        // destroy the session
        session_destroy();

        return redirect("/");

    }

}
