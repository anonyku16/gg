<?php namespace App\Controllers;

use App\Models\M_user;
use App\Entities\Entities_user as EntitiesUser;

class Pubcon_user extends BaseController
{
    protected $m_user;
    protected $userId;
    protected $userAccessToken;
    protected $userDiscordId;
    protected $userName;
    protected $userEmail;
    protected $userLevel;
    protected $userRole_Id;
             
    public function setUserId($userId) {
        $this->userId = $userId;
        return $this;
    }

    function setuserAccessToken($userAccessToken) {
        $this->userAccessToken = $userAccessToken;
    }

    function setuserDiscordId($userDiscordId) {
        $this->userDiscordId = $userDiscordId;
    }

    function setuserName($userName) {
        $this->userName = $userName;
    }

    function setuserEmail($userEmail) {
        $this->userEmail = $userEmail;
    }

    function setuserLevel($userLevel) {
        $this->userLevel = $userLevel;
    }

    function setuserRole_Id($userRole_Id) {
        $this->userRole_Id = $userRole_Id;
    }


    public function __construct(){
        $this->m_user = new M_user();
    }
    
    public function get($userId = null){
        try {
            if(isset($this->email)){
                $users = $this->m_user->where('user_email', $this->email)->asArray()->first();
            }elseif($userId){
                $users = $this->m_user->where('user_id', $userId)->first();
            }else{
                $users = $this->m_user;
                $users->asArray();
                $users->findAll();
            }
            return $users;
        } catch (\Exception $ex) {
            return false;
        }
    }

    public function getByEmail($email = null){
        try {
            $users = $this->m_user->where('user_email', $email)->first();
            return $users;
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    public function add(){
        $data = array(
            'user_accesstoken'      => $this->userAccessToken,
            'user_discordid'        => $this->userDiscordId,
            'user_name'             => $this->userName,
            'user_email'            => $this->userEmail,
            'user_level'            => $this->userLevel,
            'user_role_id'          => $this->userRole_Id
        );
        
        try {
            $saved = $this->m_user->insert($data);
            
            $data['user_id'] = $saved;
            return $data;
        } catch (\Exception $ex) {
            return $ex;
        }
    }
    
    public function edit($email = null){
        try {
            if($this->userId || $email ){
                if(!is_null($email)){
                    $user = $this->m_user->where('user_email', $email)->first();
                }else{
                    $user = $this->m_user->find($this->userId);
                }
                if(isset($this->name)){
                    $user->user_name = $this->name;
                }
                if(isset($this->password)){
                    $user->user_password = password_hash($this->password, PASSWORD_DEFAULT);
                }
                $user->user_lastupdate = date("Y-m-d H:i:s");
                $saved = $this->m_user->save($user);
                return $saved;
            }
        } catch (\Exception $ex) {
            return false;
        }
    }
    
    public function remove($email){
        //completely delete the user
        try {
            $user = $this->m_user->where('user_email', $email)->delete();
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

}