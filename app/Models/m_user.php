<?php namespace App\Models;

use CodeIgniter\Model;

class M_user extends Model
{
        protected $table      = 'user';
        protected $primaryKey = 'user_id';

        protected $returnType    = 'App\Entities\Entities_user';
        protected $useSoftDeletes = false;

        protected $allowedFields = [
                                    'user_id',
                                    'user_accesstoken',
                                    'user_discordid',
                                    'user_name',
                                    'user_email',
                                    'user_password',
                                    'user_level',
                                    'user_role_id'
                                ];

        protected $useTimestamps = true;
        protected $createdField  = 'user_createddate';
        protected $updatedField  = 'user_updateddate';

        protected $validationRules    = [
            'user_email'        => 'required|valid_email|is_unique[user.user_email]'
        ];

        protected $validationMessages = [
            'user_email'        => [
                'is_unique' => 'ERROR. email has already used.'
            ]
        ];

        protected $skipValidation     = true;
}