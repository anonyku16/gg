<!DOCTYPE html>
<html lang="en">
<head>


	
	
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta author="Aditya Rahadian K">
	
	<title>Garuda Gaming </title>

	<link href="#" rel="icon">

	<!--Plugins css-->
	<link rel="stylesheet" href="<?php echo base_url("/assets/css/plugins.css"); ?> ">

	<!--FA FONT-->
	<link rel="stylesheet" href="<?php echo base_url("/assets/fafont/css/fontawesome.css"); ?> ">
	<link rel="stylesheet" href="<?php echo base_url("/assets/fafont/css/brands.css"); ?> ">
	<link rel="stylesheet" href="<?php echo base_url("/assets/fafont/css/solid.css"); ?> ">

	<!--Custom Styles-->
	<link rel="stylesheet" href="<?php echo base_url("/assets/css/style.css"); ?> ">
	

	
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="90">

	<!--PreLoader-->
	<div class="loader">
		<div class="loader-inner">
			<div class="loader-blocks" >
			
				<span class="block-1"></span>
				<span class="block-2"></span>
				<span class="block-3"></span>
				<span class="block-4"></span>
				
				<span class="block-5"></span>
				<span class="block-6"></span>
				<span class="block-7"></span>
				<span class="block-8"></span>
				
				<span class="block-9"></span>
				<span class="block-10"></span>
				<span class="block-11"></span>
				<span class="block-12"></span>
				
				<span class="block-13"></span>
				<span class="block-14"></span>
				<span class="block-15"></span>
				<span class="block-16"></span>
				
			</div>
		</div>
	</div>
	<!--PreLoader Ends-->	

	<!-- header nav -->
	<!-- header nav -->
	
	
	<!-- INCLUDE CONTENT -->
			
	<!--Full Screen Section Video with caption-->
	<section class="full-screen parallax-video parallaxie center-block bg-video-container" id="ourhome" style="height: 541px; width: 1903px; background-image: url('assets/video/thumb-slide.jpg'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center 0px; position: relative; overflow: hidden;">
		<div class="container">
		
			<div class="row">
				<div class="col-md-2 col-sm-1"></div>
				<div class="col-md-8 col-sm-10">
				   <div class="center-item text-center video-content">
					  <h2 style="margin-bottom:50px;" class="text-capitalize bottom35  whitecolor">
                            <span style="margin-bottom:-35px;" class="font-xlight block wow fadeIn" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeIn;"> 
                                <img style="width:300px;" src="assets/images/LOGO_GARUDA.png"  style="width:56%;"> 
                            </span>
							
                            <span style="font-size:40px;" class="font-xlight block wow fadeIn" data-wow-delay="400ms" style="visibility: visible; animation-delay: 400ms; animation-name: fadeIn;">
                                <b> Let's Join Us And Play Together </b> <br>
                                <!-- Join Our Guild & Login Using Discord -->
                                <span style="font-size:23px;">Please Login through disscord to Join Our Community </span>
                                <a href="https://discordapp.com/api/oauth2/authorize?client_id=625386197044297756&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fregister&response_type=code&scope=identify%20email%20connections%20guilds%20guilds.join%20rpc" target="">
                                    <img style="width:300px; margin-top:15px;" src="assets/images/logindiscord.png">
                                </a>
                            </span>
							
					  </h2>

					   <a href="#" target="__blank"><i style="font-size:30px; color:white;" class="fab fa-youtube"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					   <a href="https://www.instagram.com/garudagaming.id/" target="__blank"><i style="font-size:30px; color:white;" class="fab fa-instagram"></i></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					   <a href="https://www.facebook.com/GRDGM" target="__blank"><i style="font-size:30px; color:white;" class="fab fa-facebook"></i></a>

				   </div>
				</div>
				<div class="col-md-2 col-sm-1"></div>
			</div>
			
		</div> 
		<video class="my-background-video jquery-background-video" loop autoplay="" muted="" poster="assets/video/thumb-slide.jpg" style="min-width: auto; min-height: auto; width: 1903px; height: auto; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%); transition-duration: 500ms;">
			<source src="assets/video/AlbionGaruda.mp4" type="video/mp4">
			<source src="assets/video/AlbionGaruda.ogv" type="video/webm">
			<source src="assets/video/AlbionGaruda.webm" type="video/ogg">
		</video>
		<button class="jquery-background-video-pauseplay play" style="left: auto; right: 15px; top: 15px; bottom: auto;"><span>Play</span></button>
	</section>
	
	   	 
	
			
	<!-- /INCLUDE CONTENT -->
	
 
	
	<!-- Rotate Text -->
	
	
	
	<script>
	
	var TxtRotate = function(el, toRotate, period) {
	  this.toRotate = toRotate;
	  this.el = el;
	  this.loopNum = 0;
	  this.period = parseInt(period, 8) || 5000;
	  this.txt = '';
	  this.tick();
	  this.isDeleting = false;
	};

	TxtRotate.prototype.tick = function() {
	  var i = this.loopNum % this.toRotate.length;
	  var fullTxt = this.toRotate[i];

	  if (this.isDeleting) {
		this.txt = fullTxt.substring(0, this.txt.length - 9);
	  } else {
		this.txt = fullTxt.substring(0, this.txt.length + 4);
	  }

	  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

	  var that = this;
	  var delta = 300 - Math.random() * 100;

	  if (this.isDeleting) { delta /= 2; }

	  if (!this.isDeleting && this.txt === fullTxt) {
		delta = this.period;
		this.isDeleting = true;
	  } else if (this.isDeleting && this.txt === '') {
		this.isDeleting = false;
		this.loopNum++;
		delta = 600;
	  }

	  setTimeout(function() {
		that.tick();
	  }, delta);
	};

	window.onload = function() {
	  var elements = document.getElementsByClassName('txt-rotate');
	  for (var i=0; i<elements.length; i++) {
		var toRotate = elements[i].getAttribute('data-rotate');
		var period = elements[i].getAttribute('data-period');
		if (toRotate) {
		  new TxtRotate(elements[i], JSON.parse(toRotate), period);
		}
	  }
	  // INJECT CSS
	  var css = document.createElement("style");
	  css.type = "text/css";
	  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
	  document.body.appendChild(css);
	};

	</script>
	

	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="assets/js/jquery-3.1.1.min.js"></script> 

	<!-- Bootstrap Core  -->
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!--to view items on reach-->
	<script src="assets/js/jquery.appear.js"></script>

	<!--Equal-Heights-->
	<script src="assets/js/jquery.matchHeight-min.js"></script>

	<!--Owl Slider-->
	<script src="assets/js/owl.carousel.min.js"></script>

	<!--number counters-->
	<script src="assets/js/jquery-countTo.js"></script>
	 
	<!--Parallax Background-->
	<script src="assets/js/parallaxie.js"></script>
	  
	<!--Cubefolio Gallery-->
	<script src="assets/js/jquery.cubeportfolio.min.js"></script>

	<!--FancyBox popup-->
	<script src="assets/js/jquery.fancybox.min.js"></script>         

	<!-- Video Background -->
	<script src="assets/js/jquery.background-video.js"></script> 

	<!--TypeWriter-->
	<script src="assets/js/typewriter.js"></script> 
		  
	<!--Particles-->
	<script src="assets/js/particles.min.js"></script>            
			
	<!--WOw animations -->
	<script src="assets/js/wow.min.js"></script> 

	<!--FAFONT -->
	<script src="assets/fafont/js/fontawesome.js"></script> 
				 
	<!--Revolution SLider-->
	<script src="assets/js/revolution/jquery.themepunch.tools.min.js"></script>
	<script src="assets/js/revolution/jquery.themepunch.revolution.min.js"></script>
	<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
	<script src="assets/js/revolution/extensions/revolution.extension.actions.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.carousel.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.kenburn.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.migration.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.navigation.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.parallax.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.slideanims.min.js"></script>
	<script src="assets/js/revolution/extensions/revolution.extension.video.min.js"></script>  
			
	<!--Google Map API
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_egfOLjahHB0IWpykRZrVFD8fN4JMgmw"></script>  -->
	
	
	
		<script src="assets/js/functions.js"></script>
	
	
	
</html>